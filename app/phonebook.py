import json
import sys
import os
import uuid


class Phonebook:
    file = None

    def __init__(self):
        self.file = os.getcwd() + '/db.json'

    def save(self, data):
        json_data = json.dumps(data)
        with open(self.file, "w") as db:
            db.write(json_data)
        db.close()

    def add_one(self, name, surname, phone):
        rec_id = str(uuid.uuid4())
        rec = {
            "id": rec_id,
            "name": name,
            "surname": surname,
            "phone": phone
        }
        book = self.read()
        book.append(rec)
        self.save(book)

    def del_one(self, rec_id):
        book = self.read()
        for rec in book:
            if rec.get("id") == rec_id:
                book.remove(rec)
                break
        self.save(book)
        print(f"Usunieto rekord o ID: {rec_id}")

    def read(self):
        with open(self.file, "r") as db:
            book = json.load(db)
        db.close()

        def sort_by(val):
            return val.get('surname')

        book.sort(key=sort_by)
        return book

    def read_one(self, rec_id):
        book = self.read()
        for rec in book:
            if rec.get("id") == rec_id:
                print(f"Znaleziono {rec_id}")
                return
        print(f"Nie znaleziono rokordu z ID: {rec_id}")
