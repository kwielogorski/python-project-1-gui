import json
import sys
import uuid
import gui as gui

file = 'db.json'
valid_commands = {
    "1": "wyświetl wszystkie wpisy",
    "2": "wyswietl wpis",
    "3": "dodaj nowy wpis",
    "4": "usun wpis",
    "?": "wyświetl opcje",
    "x": "wyjdz"
}


def display_init():
    print("Książka telefoniczna v.0.0.1")
    print("")


def display_info():
    print("")
    print("Wybierz opcję:")
    for k,v in valid_commands.items():
        print(f"[{k}] {v}")


def run_command(cmd):
    if cmd not in valid_commands:
        print("Wybrano nieistniejącą opcję, spróbuj ponownie.")
    else:
        if cmd == "1":
            display_all()
        elif cmd == "2":
            show_one(input("Podaj ID wpisu do wyswietlenia: "))
        elif cmd == "3":
            add_one()
        elif cmd == "4":
            del_one(input("Podaj ID wpisu do usuniecia: "))
        elif cmd == "x":
            print("OK, THX, BYE")
            sys.exit()
        else:
            print("not implemented yet :P")
        

def read_input():
    return run_command(input("Podaj opcję: "))


def read():
    with open(file, "r") as db:
        book = json.load(db)
    db.close()
    return book


def save(data):
    json_data = json.dumps(data)
    with open(file, "w") as db:
        db.write(json_data)
    db.close()


def display(rec):
    print("")
    print("{:>10}| {}".format("ID", rec.get("id")))
    print("{:-<45}".format(""))
    print("{:>10}| {}".format("Imie", rec.get("name")))
    print("{:>10}| {}".format("Nazwisko", rec.get("surname")))
    print("{:>10}| {}".format("Telefon", rec.get("phone")))
    print("")


def display_all():
    book = read()
    print("")
    print("{:<40}| {:<6}| {:<20}".format("ID", "Imie", "Nazwisko"))
    print("{:-<96}".format(""))
    for rec in book:
        print(
                "{:<40}| {:<6}| {:<20}".format(
                    rec.get("id"), 
                    rec.get("name")[:1]+".",
                    rec.get("surname")
                )
        )
    total = len(book)
    print(f"Lacznie wpisow: {total}")


def add_one():
    rec_id = str(uuid.uuid4())
    name = input("Imię: ")
    surname = input("Nazwisko: ")
    phone = input("Telefon: ")
    rec = {"id": rec_id, "name": name, "surname": surname, "phone": phone}
    
    book = read()
    book.append(rec)
    save(book)


def del_one(rec_id):
    book = read()
    if "t" != input("Czy na pewno? [t/N]: "):
        return
    for rec in book:
        if rec.get("id") == rec_id:
            book.remove(rec)
            break
    save(book)
    print(f"Usunieto rekord o ID: {rec_id}")


def show_one(rec_id):
    book = read()
    for rec in book:
        if rec.get("id") == rec_id:
            display(rec)
            return
    print(f"Nie znaleziono rokordu z ID: {rec_id}")


# Start
display_init()

while (True):
    display_info()
    read_input()









