from tkinter import *
from tkinter import messagebox
from functools import partial

class Gui: 
    def __init__(self):
        self.root = Tk()
        self.build()
        
    def build(self):
        menu = Menu(self.root)
        self.root.config(menu=menu)
        filemenu = Menu(menu)
        menu.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="Exit", command=self.root.quit)
        
    def run(self):
        self.root.mainloop()
        
        
gui = Gui()
gui.run()