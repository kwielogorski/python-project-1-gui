import os
from functools import partial
from tkinter import *
from tkinter import messagebox


class Gui:
    phonebook = None
    mainframe = None
    record_list = None
    records = None

    def __init__(self, phonebook):
        self.phonebook = phonebook
        self.root = Tk()
        self.root.title("Phonebook (v1.0)")
        photo = PhotoImage(file=os.path.abspath('resources/icon.gif'))
        self.root.call('wm', 'iconphoto', self.root._w, photo)
        self.build()

    def build(self):
        if self.mainframe is not None:
            self.mainframe.destroy()
        self.mainframe = Frame(
            self.root,
            bg='#3c3f41',
            padx=10
        )
        self.mainframe.grid(row=0, column=0)
        self.build_menu()
        self.build_header()
        self.build_main_view()
        self.build_footer()

    def build_menu(self):
        menu = Menu(self.root)
        self.root.config(menu=menu)

        file_menu = Menu(menu)
        menu.add_cascade(label="File", menu=file_menu)
        file_menu.add_command(label="Exit", command=self.root.quit)

        contacts_menu = Menu(menu)
        menu.add_cascade(label="Contacts", menu=contacts_menu)
        contacts_menu.add_command(label="Create New", command=self.add)

        about_menu = Menu(menu)
        menu.add_cascade(label="Help", menu=about_menu)
        about_menu.add_command(label="About", command=self.show_about)

    def build_header(self):
        Label(
            self.mainframe,
            width=50,
            text="Contacts",
            bg='#3c3f41',
            foreground='grey'
        ).grid(row=0, column=0, columnspan=2, sticky=W + E)

    def build_main_view(self):
        self.records = self.phonebook.read()

        self.record_list = Listbox(
            self.mainframe,
            selectmode=SINGLE,
            width=20,
            height=20,
            borderwidth=0
        )
        self.record_list.grid(row=1, column=0, sticky=W+E)
        for rec in self.records:
            self.record_list.insert(
                END,
                (rec.get("surname"), rec.get("name"))
            )
        self.record_list.bind('<<ListboxSelect>>', self.preview)

    def add(self):
        right_frame = Frame(
            self.mainframe,
            bg='#3c3f41',
            width=20
        )
        right_frame.grid(row=1, column=1, sticky=N + W + E)
        Label(
            right_frame,
            text="Add new:",
            bg='#3c3f41',
            foreground='grey',
            font=(None, 13, 'bold'),
            anchor=W
        ).grid(row=0, column=0, sticky=N + W + E)

        fields = {'name': None, 'surname': None, 'phone': None}
        for i, (field, entry) in enumerate(fields.items(), 1):
            Label(
                right_frame,
                text=field[0].upper() + field[1:],
                width=10,
                bg='#3c3f41',
                foreground='grey',
                padx=3,
                anchor=W
            ).grid(row=i, column=0, sticky=W)
            fields[field] = Entry(
                right_frame
            )
            fields[field].grid(row=i, column=1, sticky=W+E)
        Button(
            right_frame,
            text="Add",
            bg='#3c3f41',
            highlightbackground='#3c3f41',
            foreground='grey',
            padx=20,
            command=partial(self.add_record, fields)
        ).grid(row=len(fields.items()) + 1, column=1, sticky=W)

    def preview(self, event):
        widget = event.widget
        index = int(widget.curselection()[0])
        record = self.records[index]

        right_frame = Frame(
            self.mainframe,
            bg='#3c3f41',
            width=20
        )
        right_frame.grid(row=1, column=1, sticky=N+W+E)
        Label(
            right_frame,
            text="Details:",
            bg='#3c3f41',
            foreground='grey',
            font=(None, 13, 'bold'),
            anchor=W
        ).grid(row=0, column=0, sticky=N+W+E)

        for i, (field, value) in enumerate(record.items(), 1):
            if field == 'id':
                continue
            Label(
                right_frame,
                text=field[0].upper() + field[1:],
                width=10,
                bg='#3c3f41',
                foreground='grey',
                padx=3,
                anchor=W
            ).grid(row=i, column=0, sticky=W)
            Label(
                right_frame,
                text=value,
                bg='#3c3f41',
                foreground='grey',
                anchor=W
            ).grid(row=i, column=1, sticky=W+E)

        Frame(
            right_frame,
            width=20,
            height=15,
            bg='#3c3f41'
        ).grid(row=len(record.items())+1, column=0, columnspan=2, sticky=W+E)
        Button(
            right_frame,
            text="Delete",
            bg='#3c3f41',
            highlightbackground='#3c3f41',
            foreground='grey',
            padx=20,
            command=self.rm_record
        ).grid(row=len(record.items())+2, column=1, sticky=W)

    def build_footer(self):
        Label(
            self.mainframe,
            text="Records: {}".format(self.record_list.size()),
            bg='#3c3f41',
            foreground='grey',
            anchor=W
        ).grid(row=2, column=0, sticky=W+E)
        Label(
            self.mainframe,
            text="Database in use: " + os.path.basename(self.phonebook.file),
            bg='#3c3f41',
            foreground='grey',
            anchor=E
        ).grid(row=2, column=1, sticky=W+E)

    def run(self):
        self.root.mainloop()

    def open_file(self, file):
        self.phonebook.file = file

    @staticmethod
    def show_about():
        messagebox.showinfo('About', 'Coded with ♥ by Krzysztof Wielogórski')

    def add_record(self, fields):
        # print(name, surname, phone)
        self.phonebook.add_one(
            fields['name'].get(),
            fields['surname'].get(),
            fields['phone'].get()
        )
        self.build()

    def rm_record(self):
        if not messagebox.askokcancel("Delete contact", "Are you sure you want to delete this contact?"):
            return
        index = int(self.record_list.curselection()[0])
        self.phonebook.del_one(self.records[index].get('id'))
        self.build()



